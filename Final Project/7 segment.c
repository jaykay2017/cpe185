/*
  Blank Simple Project.c
  http://learn.parallax.com/propeller-c-tutorials 
*/
#include "simpletools.h"                      // Include simple tools

int main()                                    // Main function
{
  // Add startup code here.

 
  while(1)
  {
    int loop;
    int button = input(8);
    
    if(button == 1){
     printf("button pressed\n");
     countdown(); 
    }
    
    printf("button not pressed %d\n", loop);
    loop++;
    pause(100);
      
  }  
}


//------------
void zero(){
    high(2);
    high(1);
    high(0);
    high(3);
    high(4);
    high(5);
}
void zeroF(){
    low(2);
    low(1);
    low(0);
    low(3);
    low(4);
    low(5);
}
//------------
void one(){
    high(0);
    high(3);    
}
void oneF(){
    low(0);
    low(3);    
}      
//------------
void two(){
    high(2);
    high(1);
    high(3);
    high(4);
    high(6);
}
void twoF(){
    low(2);
    low(1);
    low(3);
    low(4);
    low(6);
}
//------------
void three(){
    high(0);
    high(1);
    high(3);
    high(4);
    high(6);
}
void threeF(){
    low(0);
    low(1);
    low(3);
    low(4);
    low(6);
}
//------------
void four(){
    high(0);
    high(5);
    high(3);
    high(6);
}
void fourF(){
    low(0);
    low(5);
    low(3);
    low(6);
}
//------------
void five(){
    high(0);
    high(1);
    high(4);
    high(5);
    high(6);
}
void fiveF(){
    low(0);
    low(1);
    low(4);
    low(5);
    low(6);
}

//

//Countdown

void countdown(){
  
    five();
    pause(1000);
    fiveF();
    pause(1000);
    //
    four();
    pause(1000);
    fourF();
    pause(1000);
    //
    three();
    pause(1000);
    threeF();
    pause(1000);
    //
    two();
    pause(1000);
    twoF();
    pause(1000);
    //
    one();
    pause(1000);
    oneF();
    pause(1000);
    //
    for(int i=0; i<=3;i++){
      zero();
      freqout(7,700,1500);
      pause(1000);
      zeroF();
      pause(1000); 
    }      
    
  
  
}  